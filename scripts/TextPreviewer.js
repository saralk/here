var TextPreviewer = function() {
    this.element = document.createElement('div');
    this.element.style.width = 'auto';
    this.element.style.display = 'inline-block';
    this.element.style.position = 'fixed';
    this.element.style.left = '-10000px';
    this.element.style.whiteSpace = 'pre';
    this.element.style.wordWrap = 'break-word';

    document.body.appendChild(this.element);
};

TextPreviewer.prototype.getMetrics = function(opts) {
    this.element.innerHTML = opts.text;
    this.element.style.maxWidth = opts.maxWidth + 'px';
    this.element.style.fontFamily = opts.fontFamily;
    this.element.style.fontSize = opts.fontSize + 'px';

    metrics = this._calculateMetrics();

    var elementClone = this.element.cloneNode(true);
    elementClone.style.position = 'static';
    metrics.preview = elementClone;

    return metrics;
};

TextPreviewer.prototype._calculateMetrics = function() {
    var metrics = {};
    metrics.width = this.element.offsetWidth;

    var lineHeight = document.defaultView.getComputedStyle(this.element).getPropertyValue('line-height').replace('px', '');

    metrics.lines = Math.round(this.element.offsetHeight / parseInt(lineHeight));
    
    this.metrics = metrics; 
    return metrics;
};
