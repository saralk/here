QUnit.test("previwer test", function(assert) {
    var previewer = new TextPreviewer();

    var metrics = previewer.getMetrics({
        text: "the quick brown fox jumped over the lazy dog",
        maxWidth: 1000,
        fontSize: 10,
        fontFamily: "Arial" 
    });

    assert.equal(metrics.width, 201, "Width");
    assert.equal(metrics.lines, 1, "Lines");
    assert.ok(metrics.preview !== previewer.element, "Preview Element");
});

QUnit.test("previwer test 1", function(assert) {
    var previewer = new TextPreviewer();

    var metrics = previewer.getMetrics({
        text: "the quick brown      fox jumped over the lazy dog",
        maxWidth: 150,
        fontSize: 12,
        fontFamily: "Courier New" 
    });

    assert.equal(metrics.width, 150, "Width");
    assert.equal(metrics.lines, 3, "Lines");
    assert.ok(metrics.preview !== previewer.element, "Preview Element");
    assert.equal(previewer.element.style.fontFamily, "'Courier New'", "Font Family");
});
