var calculateAndRender = function(opts) {
    var options = {};
    var valid = true;
    for (var i=0; i<opts.elements.length; i++) {
        var element = opts.elements[i];
        
        if (element.getAttribute('type') === 'number') {
            var num = parseInt(element.value)
            if (!isNaN(num)) { 
                element.parentNode.classList.remove('has-error');
            } else {
                valid = false;     
                element.parentNode.classList.add('has-error');
            }
        };

        options[element.getAttribute('name')] = element.value;
    }

    if (valid) {
        var metrics = opts.textPreviewer.getMetrics(options);

        opts.widthOutput.innerHTML = metrics.width + 'px';
        opts.linesOutput.innerHTML = metrics.lines;
        opts.previewElement.innerHTML = '';
        opts.previewElement.appendChild(metrics.preview);
    }

};

(function() {
    var widthOutput = document.getElementById('width-output');  
    var linesOutput = document.getElementById('lines-output');
    var previewElement = document.getElementById('preview');

    var elements = document.getElementById('form').getElementsByClassName('form-control');
    var textPreviewer = new TextPreviewer();

    var opts = {
        textPreviewer: textPreviewer,
        widthOutput: widthOutput,
        linesOutput: linesOutput,
        previewElement: previewElement,
        elements: elements 
    };

    var renderFunction = _.partial(calculateAndRender, opts);

    for (var i=0; i<elements.length; i++) {
        var element = elements[i];
        element.addEventListener('change', renderFunction);
        element.addEventListener('keyup', renderFunction);
        element.addEventListener('keydown', renderFunction);
    }

    calculateAndRender(opts);
})();
